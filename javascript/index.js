var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({ // CI
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });

  // return send({// ローカル
  //   msgType: "joinRace",
  //   data: {
  //     botId: {
  //       name: botName,
  //       key: botKey
  //     },
  //     // trackName: "keimola",
  //     trackName: "germany",
  //     // trackName: "usa",
  //     // trackName: "france",
  //     carCount: 1
  //   },
  // });

});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var angle = 0, maxAngle = 0, prevAngle = 0, crashAngle = 0, crashTimes = 0; raceEnded = 0;
  dist = 0, prevDist = 0, lapTime = 0, myPos = 0, myLane = -1, lap = 0;
var hazardousPieces = [], slowingPieces = [], switchingLeftPieces = [], switchingRightPieces = [], otherPos = [], otherLanes = [];

jsonStream.on('data', function(data) {

  // レース開始前にコースレイアウトを取得・分析
  if (data.msgType === 'gameInit') {
    for (var i = 0; i < data.data.race.track.pieces.length; i++) {
      if(i > 1 && Math.abs(data.data.race.track.pieces[i-1].angle - data.data.race.track.pieces[i].angle) >= 90 &&
          data.data.race.track.pieces[i-1].radius == 50){
        hazardousPieces.push(i);
      }else if(Math.abs(data.data.race.track.pieces[i].angle)){
        slowingPieces.push(i);
      }
      if(data.data.race.track.pieces[i].angle > 0 && data.data.race.track.pieces[i].switch){
        switchingLeftPieces.push(i);
      }else if(data.data.race.track.pieces[i].angle < 0 && data.data.race.track.pieces[i].switch){
        switchingRightPieces.push(i);
      }
    };
    console.log("hazardousPieces: " + hazardousPieces);
    console.log("slowingPieces: " + slowingPieces);
    console.log("switchingLeftPieces: " + switchingLeftPieces);
    console.log("switchingRightPieces: " + switchingRightPieces);
    // console.log(highPieces);
    // console.log(data.data.race.track.pieces);
  }

  // レース中はティックごとに順次実行される
  if (data.msgType === 'carPositions') {
    for (var i = 0; i < data.data.length; i++) {
      if(data.data[i].id.name == botName){//my car
        angle = Math.round(data.data[i].angle * 1000)/1000;
        dist = data.data[i].piecePosition.inPieceDistance;
        if(Math.abs(data.data[i].angle) > maxAngle){
          maxAngle = Math.abs(angle);
        }
        myPos = data.data[i].piecePosition.pieceIndex;
        myLane = data.data[i].piecePosition.lane.startLaneIndex;
        lap = data.data[i].piecePosition.lap;
        console.log(//data.data[i].id.name + " " + data.data[i].id.color +  " " +
          "angle:" + angle + " maxAngle:" + maxAngle + " move:" + Math.round((dist - prevDist) * 1000)/1000 +
          " pos:" + myPos + "+" + Math.round(dist * 1000)/1000 +
          " lap:" + (lap + 1) + //"/3" +
          " lane:" + data.data[i].piecePosition.lane.startLaneIndex +
          "=>" + data.data[i].piecePosition.lane.endLaneIndex +
          " lapTime:" + lapTime);
      }else{//other cars
        otherPos.push(data.data[i].piecePosition.pieceIndex);
        otherLanes.push(data.data[i].piecePosition.lane.endLaneIndex);
      }
      // console.log(myPos + otherPos + myLane + otherLanes);
      //レーン変更の判断
      // if(otherPos.indexOf(myPos) != -1 && otherLanes.indexOf(myLane) != -1){//渋滞時
      //   console.log("passing");
      //   if(myLane == 0){
      //     send({
      //       msgType: "switchLane",
      //       data: "Right"
      //     });
      //   }else{
      //     send({
      //       msgType: "switchLane",
      //       data: "Left"
      //     });
      //   }
      // }
      if(switchingLeftPieces.indexOf(myPos + 1) != -1){ //早めにレーン切り替え指示を出さないと間に合わない
        console.log("switchingLeft");
        send({
          msgType: "switchLane",
          data: "Left"
        });
      }else if(switchingRightPieces.indexOf(myPos + 1) != -1){
        console.log("switchingRight");
        send({
          msgType: "switchLane",
          data: "Right"
        });
      }

      // console.log(dist - prevDist);
      // if(data.data[i].piecePosition.pieceIndex == 34){
      //   console.log("TURBO!!");
      //   send({
      //     msgType: "turbo",
      //     data: "turbo"
      //   });
      // }

      //速度調整
      if(!raceEnded){
        setThrottle(1.0 - crashTimes / 10); //限界のドリフト角度を知るため意図的にクラッシュするごとに少しずつスピードを落とす
      }
      else if(hazardousPieces.indexOf(myPos + 3) != -1){
        setThrottle(0.001);
      }else if(crashAngle > 0 &&
        (slowingPieces.indexOf(myPos + 1) != -1 || dist - prevDist > 5.5)){
          setThrottle(Math.round((0.35 *  crashAngle / 58) * 1000) / 1000);
      }else{
        setThrottle(1.0);
      }
      //     // (Math.abs(dist - prevDist) > 7 && slowingPieces.indexOf(data.data[i].piecePosition.pieceIndex + 2) != -1) ||
      //     slowingPieces.indexOf(data.data[i].piecePosition.pieceIndex + 1) != -1　//&& dist > 50
      //     ){
      //   setThrottle(0.001);
      // }else{// 加速
      //   setThrottle(0.600 + (lap + 1) / 30);
      // }
      function setThrottle(speed){
        if(data.data[i].id.name == botName){
          console.log(speed);
          send({
            msgType: "throttle",
            data: speed
          });
        }
      }
      // send({
      //   msgType: "switchLane",
      //   data: "Right"
      // });
      prevAngle = angle;
      prevDist = dist;
        // send({
        //     msgType: "throttle",
        //     data: 0.8
        //   });

    };
    // console.log(data.data[0].angle);
  } else {
    if (data.msgType === 'join') {
      console.log('Race joined');
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'crash') {
      console.log('CRASHED! crashAngle:' + maxAngle);
      crashAngle = maxAngle;
      crashTimes++;
    } else if (data.msgType === 'lapFinished') {
      lapTime = data.data.lapTime.millis;
      console.log("lap:" + data.data.lapTime.lap + " time:" + data.data.lapTime.millis + " " +
        data.data.ranking.overall + " " + data.data.ranking.fastestLap);
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
      raceEnded = 1;
      for (var i = 0; i < data.data.results.length; i++) {
        console.log(data.data.results[i].car.name + " " + data.data.results[i].result.millis);
      };
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
